terraform {
  cloud {
    organization = "fake_app_test"

    workspaces {
      name = "try_poec_app"
    }
  }
}
